import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * 
 * @author Robyn Proffer
 *
 */

class predPlayer{
	//private node node; 
	private Player alien;
	private Player target;
	
	/**
	 *Constructor method sets up variables
	 *@param n is the node at which point the player originates
	 *@param p is the target player
	 */
	public predPlayer(node n, Player p) {
		target = p; //used for heuristic calculation
		//this.node = n;
		n.setSecCost(0);
		alien = new Player(n);
	}
	
	/**
	 * looks for the most efficient path
	 * @return a list of nodes made up of the path from the current point to the goal
	 */
	List<node> pathfind(){  //this version of predPlayer simply follows player 1
		int tentativeGScore;
		node x;
		node current;
		List<node> neighborhood;
		List<node> openSet = new ArrayList<node>();
		List<node> closedSet = new ArrayList<node>();
		openSet.add(this.getNode());
		while(openSet.size() != 0) {  //while openSet is not empty
			current = openSet.remove(0);	
			if(current.isOccupied()) { //if current = goal
				//System.out.println("You found player 1"); check to make sure overridden correctly
				return this.reconstructPath(current);}
			closedSet.add(current);
			neighborhood = current.getNeighbors();
			for(int i = 0; i < neighborhood.size(); i++) {
				x = neighborhood.get(i); //look at each neighbor
				if(!closedSet.contains(x)) { //if x is not in the closed set, get the gScore
					tentativeGScore = current.getSecCost() + x.getValue();
					if(!openSet.contains(x))//if neighbor not in openSet, Discover a new node
						openSet.add(x);
					openSet.sort(Comparator.comparing(nodeInterface::getSecondaryFScore));
					if(tentativeGScore <= x.getSecCost()) { //this is weird and I probably need to fix something. //measure for the shortest path from here to start
						x.setPrevious(current);
						x.setSecCost(tentativeGScore);
						x.setSecondaryFScore(x.getSecCost() + this.calculateHeuristic(x));}
				}
			}
		}
		return null;
}
	/**
	 * performs the SLD calculation to find the heuristic value
	 * @param n a given node against which to find the Straight Line Distance for creating the heuristic
	 * @return The Straight Line Distance as a double
	 */
	double calculateHeuristic(node n) {
		return Math.sqrt(Math.pow(target.getNode().getX()-n.getX(),2)+Math.pow(target.getNode().getY()-n.getY(),2));
	}
	
	/**
	 * rebuilds the path
	 * @param current
	 * @return the path made up of the previous node using the parent of the current node
	 */			
	List<node> reconstructPath(node current) { //build path to allow for selection of a move
		return alien.reconstructPath(current);
	}
	
	/**
	 * prints the path
	 * @param path a path to be printed
	 */
	void printPath(node[] path) {
		alien.printPath(path);
	}
	
	/**
	 * performs the movement action
	 * @return 1 if entering an occupied space and 0 if not
	 */
	int move() { //take the first node from path, move there. check if it's the goal. 
		System.out.print("(" + this.getNode().getX() + "," + this.getNode().getY() + ") " + "player 2");
		System.out.println();
		List<node> path = this.pathfind(); //find a path
		if(path.size() == 1) {
			this.setNode(path.remove(0));
		}
		else {
			this.setNode(path.remove(1)); //get the first one after the node you are one, p[0] is current node
		}
		if(this.getNode().isOccupied()) { //you won!!!!
			System.out.println("Player 2 won!");
			return 1;
		}
		return 0; //figure out a better return value?
	}
	
	/**
	 * get the occupied node
	 * @return the current node
	 */
	node getNode() { //return the node that this player is occupying
		return alien.getNode();
		}
	
	/**
	 * set the current node
	 * @param n a node for this player to occupy
	 * 
	 */
	protected void setNode(node n) { //set the node n to the player's node value
		alien.setNode(n);
	}
	
	/**
	 * perform a test
	 */
	void testRide() { //test
		alien.testRide();
		}
}
		