import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Robyn Proffer
 *
 */
public interface nodeInterface {
    int value = 0;
    boolean occupied = false;
    int tempV = 0;
    boolean goal = false;
    node cameFrom = null;
    List<node> neighbors = new ArrayList<node>();
    int cost = 0;
    double fScore = -0.1;
    int x = 0;
    int y = 0;
    int secCost = 0;
    double secFScore = -0.1;
    
    /**
     * get the x value
     * @return int cartesian x value
     */
    int getX();
    
    /**
     * get the y value
     * @return int cartesian y value
     */
    int getY();
    
    /**
     * set the X value
     * @param n int to be set as the X value
     */
    void setX(int n);
    
    /**
     * set the Y value
     * @param n int to be set as the Y value
     */
    void setY(int n);
    
    /**
     * check if a node is occupied
     * @return boolean true if the node being checked is occupied
     */
    boolean isOccupied();
    
    /**
     * set the parent node
     * @param n node from which the player came from
     */
    void setPrevious(node n);
    
    /**
     * get the parent node
     * @return node from which the node was entered
     */
    node getPrevious();
    
    /**
     * kill the parent
     */
    void knockPrevious();
    
    /**
     * return the F Score for player 1
     * @return double value of the F score as determined by A*
     */
    double getFScore();
    
    /**
     * get the f score for player 2
     * @return double value of the F score as determined by A*
     */
    double getSecondaryFScore();
    
    /**
     * sets the f score for use by player 2
     * @param d double value to set as secondary f score
     */
    void setSecondaryFScore(double d);
    
    /**
     * sets the f score
     * @param d double value to set as f score 
     */
    void setFScore(double d);

    /**
     * get the value
     * @return int value of the node (between 1 and 9)
     */
    int getValue();

    /**
     * set the node's value
     * @param n int value that is node's value (between 1 and 9) 
     */
    void setValue(int n);
        
    /**
     * adds a neighbor
     * @param member a node to be added as a neighbor
     */
    void addNeighbor(node member);
    
    /**
     * get the neighbors
     * @return a list of neighboring nodes
     */
    List<node> getNeighbors();
    
    /**
     *  mark the node as occupied
     */
    void occupy();

    /**
     * mark the node as unoccupied
     */
    void deoccupy();
    
    /**
     * checks if a node is the goal
     * @return boolean true if the node is the goal
     */
    boolean isGoal();

    /**
     * sets a node as the goal
     */
    void setGoal();
    
    /**
     * stages a value for replacement
     * @param n int value for staging a replacement
     */
    void setTempV(int n);
    
    /**
     * replaces value with temp value
     */
    void smooth();
        
    /**
     * set the current lowest cost to get here
     * @param n int value that is the lowest cost to get to this point
     */
    void setCost(int n);
        
    /**
     * set current lowest cost for player 2
     * @param n int cost to be set as cost to get here
     */
    void setSecCost(int n);
        
    /**
     * get cost to get here for p2
     * @return int cost to get here
     */
    int getSecCost();
        
    /**
     * get the cost to get here
     * @return int cost to get here
     */
    int getCost();
}
