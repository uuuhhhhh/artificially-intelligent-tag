
import java.util.Scanner;

/**
 * 
 * @author Robyn Proffer
 *
 */
public class Main {
	gridMap world;
	

	public static void main(String[] args) { //TODO research threading and redo the algorithms to utilize it
		Main m = new Main();
		m.run();	
	}

	void run() {
		//test = tester();
		//test.tester();
		int m = 0;
		int n = 0;
		int count;
		int devalue;
		int value;
		Scanner scanner = new Scanner(System.in);
		String p;
		//tst = node(2);
		//tst.testRide();
		//set up game;
		while(m < 5 || m > 2500) {
			System.out.println("Give me the number of rows (5 < x < 2500)");
			m = scanner.nextInt();}
		while(n < 5 || n > 2500) {
			System.out.println("Give me the number of columns (5 < x < 2500)");
			n = scanner.nextInt();}
		world = new gridMap(m, n);
		world.build();
		do {
			System.out.println("Would you like to edit the map? you may enter yes or no to indicate your response. please."); //why does this print  twice?
			p = scanner.nextLine(); //why don't you stop here? why print twice?
		}while(!p.equalsIgnoreCase("yes") && !p.equalsIgnoreCase("no"));
		if(p.equalsIgnoreCase("yes"))//strings can't do that
			world.edit();
		scanner.close();
		node playerStartNode = world.randNode();
		playerStartNode.occupy();
		playerStartNode.setCost(0);
		Player pOne = new Player(playerStartNode);
		predPlayer pTwo = new predPlayer(world.randBad(), pOne);
		world.swoll(pTwo.getNode().getX(), pTwo.getNode().getY(), (m*n)/4, 5);
		value = 0;
		devalue = 0;
		count = 0;
		while(value == 0 && devalue == 0) { //run game
			count++;
			value = pOne.move(); 
			world.reset(); //clears cameFrom values in nodes
			world.smoll(pTwo.getNode().getX(), pTwo.getNode().getY(), (m*n)/10, 10);  //remove p2 influence
			devalue = pTwo.move();
			world.swoll(pTwo.getNode().getX(), pTwo.getNode().getY(), (m*n)/10, 10); //institute p2 influence
			world.reset(); //resets node costs
		}
		System.out.printf("moves: " + count);}
	}

