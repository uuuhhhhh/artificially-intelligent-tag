import java.util.List;

/**
 * 
 * @author Robyn Proffer
 *
 */
public class naivePredator {
	private node node;
	private predPlayer alien;
	public naivePredator(node n, Player p1) {
		this.node = n;
		this.node.setSecCost(0);
		alien = new predPlayer(n, p1);
	}
	
	/**
	 * finds a path
	 * @return a list of nodes making a path
	 */
	List<node> pathfind(){  
		return null; //naivePredator has no A* influence and uses only an analysis of how many times player 1 has moved in any given direction
		//build a tree of movement nodes and analyze for how often that direction is chosen. 
	}
	
	/**
	 * reconstruct the path using the camefrom value of each node
	 * @param current the node from which to start getting the previous node to build the path
	 * @return return an arraylist containing a path
	 */
	List<node> reconstructPath(node current) { //build path to allow for selection of a move
		return alien.reconstructPath(current);
	}
	
	/**
	 * print the path coordinates
	 * @param path to print
	 */
	void printPath(node[] path) {
		alien.printPath(path);
	}
	
	/**
	 * perform the move action
	 * @return 1 if the move results in winning else a 0
	 */
	int move() { //take the first node from path, move there. check if it's the goal. 
		System.out.print("(" + this.getNode().getX() + "," + this.getNode().getY() + ") " + "player 2");
		System.out.println();
		//List<node> path = this.pathfind(); //find a path
		//not sure how movement will work yet
		if(this.getNode().isOccupied()) { //you won!!!!
			System.out.println("Player 2 won!");
			return 1;
		}
		return 0; //figure out a better return value?
	}
	
	/**
	 * return the current node
	 * @return the current occupied node
	 */
	node getNode() { //return the node that this player is occupying
		return alien.getNode();
		}
	
	/**
	 * set a node as the current node
	 * @param n the node to set as the current node
	 */
	void setNode(node n) { //set the node n to the player's node value
		alien.setNode(n);
	}
	
	/**
	 * tester
	 */
	void testRide() { //test
		alien.testRide();
		}
}
