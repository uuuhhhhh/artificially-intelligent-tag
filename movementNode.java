// movementNode class with get and set methods

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Robyn Proffer
 *
 */

public class movementNode {
	private boolean isLeaf;
	private movementNode left; //keeping with convention, this will the the ideal node
	private movementNode right; //keeping with convention, this will be the second most ideal 
	private movementNode parent;
	private gridMap worldState;
	private Player target; 
	node playerPosition;
	List<node> path = new ArrayList<node>(); 
	
	   /**
	    * Sets the area code
	    * @param 
	    */
	public movementNode(gridMap state, node position, List<node> path) {
		this.worldState = state;
		this.playerPosition = position;
		path.add(position); //change this so you're passing the path to the children
	}
	
	   /**
	    * Sets the area code
	    * @param area the area code
	    */
	void addleft(movementNode possibility) {
		this.left = possibility;
	}
	   /**
	    * Sets the area code
	    * @param area the area code
	    */
	void addright(movementNode possibility) {
		this.right = possibility;
	}
	
	/**
	 * Sets the area code
	 * @param area the area code
	 * @return Exception in case of invalid area code
	 */
	boolean isLeaf() {
		return (this.right == null && this.left == null);
	}
	
	/**
	 * Sets the area code
	 * @param area the area code
	 */
	movementNode getParent() {
		return this.parent;
	}
	
	/**
	 * Sets the area code
	 * @param area the area code
	 */
	void setParent(movementNode adult) {
		this.parent = adult;
	}
	
	
	
}







