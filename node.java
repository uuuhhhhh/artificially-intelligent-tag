import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Robyn Proffer
 *
 */
class node implements nodeInterface{
    private int value;
    private boolean occupied;
    private int tempV;
    private boolean goal;
    private node cameFrom;
    private List<node> neighbors = new ArrayList<node>();
    private int cost;
    private double fScore;
    private int x;
    private int y;
    private int secCost;
    private double secFScore;
    

    public node(int n) {
        this.value = n;
        this.occupied = false;
        this.tempV = 0;
        this.goal = false;
        //this.cameFrom = null;
        //this.neighbors = null;  //make this a set
        this.cost = 99999;
        this.fScore = 0.0;
        this.x = 0;
        this.y = 0;
        this.secCost = 99999;
        this.secFScore = 0.0;
    }
        

    public int getX() { // get x-value
        return this.x;
	}
    

    public void setX(int n) { //set x-value
        this.x = n;
    }
    

    public int getY() {//get y-value
        return this.y;
    }
    
    
    public void setY(int n) { //set y-value
        this.y = n;
    }

    
    public boolean isOccupied() { //check if the node is occupied by a player
        return this.occupied;
    }
    
    
    public void setPrevious(node n) { //set the node from which this node was reached
        this.cameFrom = n;
    }
    
    
    public node getPrevious() { //return the node from which this node was reach
        return this.cameFrom;
    }
    
    public void knockPrevious() { //do I need this?
        this.cameFrom = null;
    }
    
    public double getFScore(){ //return fScore for player 1
        return this.fScore;
     }
    
    public double getSecondaryFScore() { //return fScore for player 2
        return this.secFScore;
     }
    
    public void setSecondaryFScore(double d) { //set the fScore for player 2
        this.secFScore = d;
    }
    
    public void setFScore(double d) { //set fScore for player 1
        this.fScore = d;
    }

    public int getValue() { //return the value of this node
        return this.value;
    }

    public void setValue(int n) { //set the value as n
        this.value = n;
    }
        
    public void addNeighbor(node member) { //add a neighbor
        if(!this.neighbors.contains(member)) {
            this.neighbors.add(member);
        }
    }
    
   public List<node> getNeighbors() { //return the array of neighbors
        return this.neighbors;
    }
    
    public void occupy() { //designate the node as occupied
        this.occupied = true;
    }

    public void deoccupy() { //designate the node as unoccupied
        this.occupied = false;
    }
    
    public boolean isGoal() { //check to see if this is the goal
        return this.goal;
    }

    public void setGoal() { //designate the node as the goal
        this.goal = true;
    }
    
    public void setTempV(int n) { //set a temporary value with which to replace the value in a moment
        this.tempV = n;
    }
    
    public void smooth() { //replace the value with the tempValue
        this.value = this.tempV;
    }
        
    public void setCost(int n) { //set the cost to get here from start
        this.cost = n;
    }
        
    public void setSecCost(int n) { //set the cost to get here from player 2 start
        this.secCost = n;
    }
        
    public int getSecCost() { //return the cost to get here from player 2 start
        return this.secCost;
    }
        
    public int getCost() {//return the cost to get here from start
        return this.cost;
    }

}
    /**def testRide(self): //test things
        probably test all the set and get methods. 
        self.setCost(10)
        if(self.getCost() != 10):
            print("fail on get or set cost in node")
        self.setTempV(10)
        if(self._tempV != 10):
            print("fail on setTempV")
        self.setGoal()
        if(self.isGoal() != True):
            print("goal failure")
        self.occupy()
        if(self.isOccupied() != True):
            print("occupation failure") 
        self.deoccupy()
        if(self.isOccupied() != False):
            print("deoccupy fail")
        self.setHToPrey(10)
        if(self.getHToPrey() != 10):
            print("h to prey fail")
        self.setHValue(10)
        if(self.getHValue() != 10):
            print("hValue fail")
        self.setValue(10)
        if(self.getValue() != 10):
            print("value fail")
        self.setFScore(10)
        if(self.getFScore() != 10):
            print("fScore fail")        
        self.setX(10)
        self.setY(10)
        if(self.getX() != 10):
            print("x fail")
        if(self.getY() != 10):
            print("y fail")
        x = node(10)
        self.addNeighbor(x)
        y = self.getNeighbors()
        if(x not in y):
            print("neighbor fail")
        self.setPrevious(x)
        if(self.getPrevious() != x):
            print("previous fail")
        print("done")
        }
        */



