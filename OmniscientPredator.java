import java.util.List;

/**
 * 
 * @author Robyn Proffer
 *
 */

public class OmniscientPredator { //isn't actually smart just knows Player1's pathfinding algorithm
	private node node;
	private predPlayer alien;
	
	/**
	 * constructor
	 * @param n node to start on
	 * @param p1 player to seek out
	 */
	public OmniscientPredator(node n, Player p1) {
		this.node = n;
		this.node.setSecCost(0);
		alien = new predPlayer(n, p1);
	}
	
	/**
	 * build a path
	 * @return a list of nodes containing a path
	 */
	List<node> pathfind(){  
		return null; //OmniscientPredator has A* influence on it's pathfinding algorithm. It knows that player 1 uses the A* algorithm and uses that to predict its movement
	//build a tree of movementNodes
	//this is not recursive: you don't need to remake player 2 each time just assume that player 2 moved to a point and let the worldstate reflect this
	}
	
	/**
	 * rebuild the path using the camefrom attribute of each node in the path
	 * @param current the node to start building from
	 * @return a list of nodes to traverse as a path
	 */
	List<node> reconstructPath(node current) { //build path to allow for selection of a move
		return alien.reconstructPath(current);
	}
	
	/**
	 * 
	 * @param path to print
	 */
	void printPath(node[] path) {
		alien.printPath(path);
	}
	
	/**
	 * perform the move action
	 * @return int 1 if a winning move else a 0
	 */
	int move() { //take the first node from path, move there. check if it's the goal. 
		System.out.print("(" + this.getNode().getX() + "," + this.getNode().getY() + ") " + "player 2");
		System.out.println();
		//List<node> path = this.pathfind(); //find a path
		//not sure how movement will work yet
		if(this.getNode().isOccupied()) { //you won!!!!
			System.out.println("Player 2 won!");
			return 1;
		}
		return 0; //figure out a better return value?
	}
	
	/**
	 * get the currently occupied node
	 * @return the node currently occupied
	 */
	node getNode() { //return the node that this player is occupying
		return alien.getNode();
		}
	
	/**
	 * set a node as the current node
	 * @param n the node to be set as the current node
	 */
	void setNode(node n) { //set the node n to the player's node value
		alien.setNode(n);
	}
	
	/**
	 * tester
	 */
	void testRide() { //test
		alien.testRide();
		}
}
