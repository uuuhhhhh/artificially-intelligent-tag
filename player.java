import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 
 * @author Robyn Proffer
 *
 */
class Player{
	private node node;
	
	/**
	 * constructor
	 * @param n node at which player starts
	 */
	public Player(node n) {
		this.node = n;
	}
	
	/**
	 * finds the most cost efficient path to a goal
	 * @return a list of nodes comprising a path between two points
	 */
	List<node> pathfind() {  //this is the A* algorithm that finds a path from the given node to the goal
		List<node> neighborhood; //neighbors of each node
		node x; //node taken from neighborhood list
		node current; //the current node
		int tentativeGScore; //
		this.node.setCost(0); 
		List<node> openSet = new ArrayList<node>();
		List<node> closedSet = new ArrayList<node>(); 
		openSet.add(this.node);
		while(openSet.size() != 0) {  //while openSet is not empty
			current = openSet.remove(0);	//get the first node
			if(current.isGoal()) { //if current = goal
				//System.out.println("you found the goal. Now you just have to get there")
				return this.reconstructPath(current);  //rebuild the path and return as an array
			}
			//openSet.remove(current)
			closedSet.add(current);  //take current from openSet and add it to closedSet
			neighborhood = current.getNeighbors(); //get your neighbors
			for(int i = 0; i < neighborhood.size(); i++) {
				x = neighborhood.get(i);
				//look at each neighbor
				if(!closedSet.contains(x)) { //if x is not in the closed set, get the gScore
					tentativeGScore = current.getCost() + x.getValue();  //cost to get here plus cost to get to neighbor
					if(!openSet.contains(x)) {//if neighbor not in openSet, Discover a new node
						openSet.add(x);}
					openSet.sort(Comparator.comparing(nodeInterface::getFScore)); //test this
					if(tentativeGScore <= x.getCost()) {//measure for the shortest path from here to start
						x.setPrevious(current); //set previous node so you know where you came from
						x.setCost(tentativeGScore); //set the more efficient cost
						x.setFScore(x.getCost() + this.calculateHeuristic(x));} //set the fScore so you can sort
				}
			}
		}
		return null; //you dun goofed, somehow. not sure how. good luck
	}
	
	/**
	 * calculates the straight line distance between a node and the goal
	 * @param n node to calculate the distance to the goal from
	 * @return a double of the straight line distance
	 */
	double calculateHeuristic(node n) {//you could make this work in such a way that you could pass it a target so that you can just pass it to this in wrapping classes
		return Math.sqrt(Math.pow(0-this.getNode().getX(), 2)+Math.pow(0-this.getNode().getY(), 2));
	}

	/**
	 * rebuilds the path of previous nodes
	 * @param current node from which to build a path
	 * @return list of nodes making up the path
	 */
	List<node> reconstructPath(node current) { //build path to allow for selection of a move
		List<node> totalPath = new ArrayList<node>(); //current is goal
		totalPath.add(current);
		while(current.getPrevious() != null) {  //while you haven't gotten to the beginning
			current = current.getPrevious(); //get the previous one, this is why Node.knockPrevious exists
			totalPath.add(current);} //add it to the path
		Collections.reverse(totalPath); //path created in reverse
		return totalPath;
		}
	
	/**
	 * prints path node coordinates
	 * @param path list of nodes of which to print coordinates
	 */
	void printPath(node[] path) {
		String line;
		for(int i = 0; i < path.length; i++) {
			line = "(" + String.valueOf(path[i].getX()) +"," + String.valueOf(path[i].getY()) +")";
			System.out.println(line);}
	}
	
	/**
	 * performs the move action
	 * @return int 1 if a win else a 0
	 */
	int move() { //take the first node from path, move there. check if it's the goal. you could implement a movement points system to play off of the nodes having a cost
		this.getNode().deoccupy();  //unoccupy your current node
		System.out.print("(" + this.getNode().getX() + "," + this.getNode().getY() + ") " + "player 1");
		System.out.println();
		List<node> path = this.pathfind(); //find a path
		this.setNode(path.remove(1)); //get the first one after the node you are one, p[0] is current node
		this.getNode().occupy(); //occupy the next one
		if(this.getNode().isGoal()) { //you won!!!!
			System.out.println("Player 1 won!");
			return 1;
		}
		return 0; //figure out a better return value?
	}
	
	/**
	 * moves to a specified node
	 * @param n node to which the player should be moved
	 */
	void move(node n) {
		this.setNode(n);
	}

	/**
	 * gets the current node
	 * @return the current node
	 */
	node getNode() { //return the node that this player is at
		return this.node;
	}
	
	/**
	 * sets a node as the current position
	 * @param n the node to be set as the current position
	 */
	protected void setNode(node n) { //set the node n to the player's node value //how do I make something
		this.node = n;
	}
	
	/**
	 * unit test method
	 */
	void testRide() { //test 
		System.out.println("start");
		gridMap x = new gridMap(100, 100);
		x.build();
		List<node> testArray = new ArrayList<node>();
		node imp;
		for(int i = 0; i < 100; i++) {
			imp = new node(10);
			testArray.add(imp);
		}
		for(int a = 0; a < testArray.size(); a++) {
			testArray.get(a).setFScore(100-a); //insure disordered entries
		}
		System.out.println("unsorted array");
		for(int b = 0; b < testArray.size(); b++) {
			System.out.println(testArray.get(b).getFScore());
		}
		//this.sort(testArray);
		testArray.sort(Comparator.comparing(nodeInterface::getFScore));
		System.out.println("sorted array");
		for(int c = 0; c < testArray.size(); c++) {
			System.out.println(testArray.get(c).getSecondaryFScore()); //I know it works up to here
		node start = x.randNode();
		System.out.println("x and y values of start node");
		System.out.println(start.getY());
		System.out.println(start.getX());
		List<node> testPath = this.pathfind();
		for(int i = 0; i < testPath.size(); i++) {
			System.out.println("x and y values");
			System.out.println(testPath.get(i).getX());
			System.out.println(testPath.get(i).getY());
			System.out.println("");
		}
		System.out.println("");
		node peep = this.getNode();
		System.out.printf("X value before move", peep.getX());
		System.out.printf("Y value before move", peep.getY());
		this.move();
		node pipe = this.getNode();
		System.out.printf("X value after move", pipe.getX());
		System.out.printf("Y value after move", pipe.getY());
		if(peep.getX() == pipe.getX() && peep.getY() == pipe.getY()) {
		}
			System.out.println("movement failure"); //I think you need to make a new err object
		}
		int counter = 0;
		while(this.getNode().isGoal() != true) {
			counter++;
			this.move();
		}
		System.out.println("number of moves from beginning to end");
		System.out.println(counter);
		System.out.printf("X value after move", this.node.getX());
		System.out.printf("Y value after move", this.node.getY());
		}
}
		