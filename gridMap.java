import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * 
 * @author Robyn Proffer
 *
 */
class gridMap {
	/*
	 *  make a NodeData class, you can make an ArrayList<NodeData> and access it in O(1) with the added bonus
	 *  of now having a natural correspondence between nodes and indexes
	 *  Do this in conjunction with an adjacency list
	 * 
	 */

		private List<List<node>> field;
		private int row;
		private int column;
		
		/**
		 * constructor
		 * @param m number of rows
		 * @param n number of columns
		 */
		public gridMap(int m, int n) { //set up
			this.row = n;
			this.column = m;
			field = new ArrayList<List<node>>(this.column);
			node temp;
			//Random r = new Random();
			for(int x = 0; x < this.row; x++) {
				List<node> xList = new ArrayList<node>(this.column);
				field.add(x, xList);
				for(int y = 0; y < this.column; y++) {
					//build the field
					System.out.print(x);
					System.out.print(y);
					//temp = new node(r.nextInt(8)+1); //TODO when you finish setting up the predator stuff, create a repository branch and just set these to zero
					temp = new node(0);
					//field.get(x).add(y, temp); //add the node at x,y
					field.get(x).add(y, temp);
				}
			}
		}
		
		/**
		 * sets up the goal and gives each node its coordinates
		 */
		protected void build() {
			node z;
			for(int i = 0; i < this.field.size(); i++) {
				for(int c = 0; c < this.field.get(i).size(); c++) {
					z = this.field.get(i).get(c);
					if(i == 0 & c == 0) {
						z.setGoal();
					}
					z.setX(c); //coordinates
					z.setY(i);
					//z.setHValue(Math.sqrt(Math.pow(0-i, 2)+Math.pow(0-c, 2))); //heuristic for p1
				}
			}
			this.smooth();
		 }
		
		/**
		 * Checks with the User if they wish to edit the map and then performs that edit
		 */
		protected void edit() { //allow edits build this in later, get it working first (It will need to display the nodes in a map)
			//get input from user, and then get that node and change the value
			int a = -1;
			int b = -1;
			int c = -1;
			int d = -1;
			Scanner scanner = new Scanner(System.in);
			this.toString();
			while((a < 0 || a > this.field.size()) != false) {
				System.out.println("Give me the column index"); //add try catch
				a = scanner.nextInt();
				//print(a)
			}
			while((b < 0 || b > this.field.get(0).size()) != false) {
				System.out.println("Give me the row index"); //add try catch
				b = scanner.nextInt();
			}
			while((c < 1 || c > 5)) { //allow dynamic radius based on total map size
				System.out.println("What length of radius to you want to impact? (between 1 and 5)"); //add try catch
				c = scanner.nextInt();
			}
			//while((d < 0 || d > 9) & isinstance(d, int) != false) {
			while((d < 0 || d > 9)) {// & d instanceof Integer) {
				System.out.println("Enter a value, between 0 and 9, with which to replace it"); //add try catch
				d = scanner.nextInt();
			}
			scanner.close();
			node z = this.field.get(a).get(b);
			int l = z.getValue();
			z.setValue(d);
			if(l > d)
				{this.smoll(a, b, c, d);}
			else
				{this.swoll(a, b, c, d);}
				
			this.toString();
		}
		/**
		 * smooths out the map to prevent large changes in value across nodes when the gridmap is set up to generate nodes with random values
		 */
		private void smooth() {
			for(int i = 0; i < this.field.size(); i++) {
				for(int j = 0; j < this.field.get(i).size(); j++) {
					if(i==0) { //first row
						if(j==0) { //beginning of first row
							this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i+1).get(j).getValue() + this.field.get(i).get(j+1).getValue())/3); //get average of node values repeat all the way down
							this.field.get(i).get(j).addNeighbor(this.field.get(i+1).get(j));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j+1));
						}
						else if(j==this.field.get(i).size() - 1){ //end of first row
							this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i).get(j-1).getValue() + this.field.get(i+1).get(j).getValue())/3);
							this.field.get(i).get(j).addNeighbor(this.field.get(i+1).get(j));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j-1));
						}
						else { //first row, neither first nor last column
							this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i).get(j-1).getValue() + this.field.get(i+1).get(j).getValue() + this.field.get(i).get(j+1).getValue())/4);
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j-1));
							this.field.get(i).get(j).addNeighbor(this.field.get(i+1).get(j));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j+1));
						}
					}
					else if(i == this.field.size()-1) { //last row
						if(j==0) { //last row first column
							this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i-1).get(j).getValue() + this.field.get(i).get(j+1).getValue())/3);
							this.field.get(i).get(j).addNeighbor(this.field.get(i-1).get(j));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j+1));
						}
						else if(j == this.field.get(i).size()-1) { //last row last column
							this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i).get(j-1).getValue() + this.field.get(i-1).get(j).getValue())/3);
							this.field.get(i).get(j).addNeighbor(this.field.get(i-1).get(j));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j-1));
						}
						else { //last row and neither first nor last column
							this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i).get(j-1).getValue() + this.field.get(i-1).get(j).getValue() + this.field.get(i).get(j+1).getValue())/4);
							this.field.get(i).get(j).addNeighbor(this.field.get(i-1).get(j));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j-1));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j+1));
						}
					} //this is the one if it throws an  error later
					else if(j==0 & i != 0 & i != this.field.size()-1) { //first column and neither first row nor last row
						this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i-1).get(j).getValue() + this.field.get(i+1).get(j).getValue() + this.field.get(i).get(j+1).getValue())/4);
						this.field.get(i).get(j).addNeighbor(this.field.get(i+1).get(j));
						this.field.get(i).get(j).addNeighbor(this.field.get(i-1).get(j));
						this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j+1));
					}
					else if(j==this.field.get(i).size()-1 & i != 0 & i != this.field.size()-1) { //last column but not top or bottom row
						this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() +this.field.get(i-1).get(j).getValue() + this.field.get(i+1).get(j).getValue() + this.field.get(i).get(j-1).getValue())/4);
						this.field.get(i).get(j).addNeighbor(this.field.get(i+1).get(j));
						this.field.get(i).get(j).addNeighbor(this.field.get(i-1).get(j));
						this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j-1));
					}
					else {	//middle nodes
						this.field.get(i).get(j).setTempV((this.field.get(i).get(j-1).getValue() + this.field.get(i-1).get(j).getValue() + this.field.get(i+1).get(j).getValue() + this.field.get(i).get(j+1).getValue())/4);
						this.field.get(i).get(j).addNeighbor(this.field.get(i+1).get(j));
						this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j+1));
						this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j-1));
						this.field.get(i).get(j).addNeighbor(this.field.get(i-1).get(j));
					}
					}
				}
			for(int i = 0; i < this.field.size(); i++) {
				for(int j = 0; j < this.field.get(i).size(); j++){
					this.field.get(i).get(j).smooth();} //switching value for temp value
			}
		}
		
		/**
		 * resets each node's cameFrom value to allow for the next round of pathfinding
		 */
		void reset() {
			for(int i = 0; i < this.field.size(); i++) {
				for(int j = 0; j < this.field.get(i).size(); j++){ {
					this.field.get(i).get(j).knockPrevious();}  //reset path; prevents standstill
			}
		}
		}
		
		/**
		 * get a node
		 * @return a random node in the world
		 */
		node randNode() { //returns a random node for player placement
			Random r = new Random();
			List<node> rowHeads = this.field.get(r.nextInt(this.field.size()));
			return rowHeads.get(r.nextInt(rowHeads.size()));
		}
		
		/**
		 * get a node
		 * @return a random node or the node in the middle depending. 
		 */
		node randBad() {
			return this.field.get((int) Math.floor(this.field.size()/2)).get((int) Math.floor(this.field.size()/2));
		}
		
						
		/*#def search(this, centerx, centery, radius):
			#for i in range(centerx - radius, centerx + radius):
				#for j in range(centery - radius, centery + radius):
					#if(i < 0 or j < 0):
						#break
					#if(i > len(this.field) or j > len(this.field[i])):
						#break
					#if(i == centery or j == centerx):
						#break
					#if(this.field.get(i).get(j).isOccupied == true):
						#return true
		//get the position of a player and search within the radius for other players
		 * 
		 */
		
		/**
		 * swells the map		
		 * @param centerx the x value of the center of the circle you want to change 
		 * @param centery the y value of the center of the cirlce you want to change
		 * @param radius the radius in which to make changes
		 * @param amount the amount to be added to the node value (caps at 9)
		 */
		void swoll( int centerx, int centery, int radius, int amount) { //swell an area
			for(int i = (int) Math.ceil(centerx - radius); i < Math.ceil(centerx + radius); i++) {//check whether, in python, the range for for loops is inclusive or exclusive also check your cast to int: does it go up or down
				for(int j = (int) Math.ceil(centery - radius); j < Math.ceil(centery + radius); j++) {//check whether, in python, the range for for loops is inclusive or exclusive also check your cast to int: does it go up or down
					if(i < 0 || j < 0)
						{break;}
					if(i > this.field.size() || j > this.field.get(i).size())
						{break;}
					this.field.get(i).get(j).setValue(Math.min(9,this.field.get(i).get(j).getValue()+(amount-(Math.max(i,j)/2))));
				}
			}	
		}
					
		/**
		 * negatively swells the map	
		 * @param centerx the x value of the center of the circle you want to change 
		 * @param centery the y value of the center of the circle you want to change
		 * @param radius the radius in which to make changes
		 * @param amount the amount to be subtracted from the node value (floors at 0)
		 */
		void smoll(int centerx, int centery, int radius, int amount) { //negatively swell an area
			for(int i = (int) Math.ceil(centerx - radius); i < Math.ceil(centerx + radius); i++) {
				for(int j = (int) Math.ceil(centery - radius); j < Math.ceil(centery + radius); j++) {
					if(i < 0 || j < 0)
						break;
					if(i > this.field.size() || j > this.field.get(i).size())
						break;
					this.field.get(i).get(j).setValue(Math.max(0,this.field.get(i).get(j).getValue()-(amount+(Math.max(i,j)*2)))); 
				}
			}
		}
		
		/**
		 * prints the map 
		 */
		public void print() { //print the thing
			String a = "  " ;
			String p = "";
			for(int b = 0; b < this.field.size(); b++) {
				a += String.valueOf(b)+ "_";
			}
			System.out.println(a);
			for(int i = 0; i < this.field.size(); i++) {
				p = String.valueOf(i) + "|";
				for(int j = 0; j < this.field.get(i).size(); j++) {
					p += " "+ String.valueOf((Math.floor(this.field.get(i).get(j).getValue())));}
				System.out.println(p);
				System.out.println("");
			}
		}
		
		/**
		 * makes sure there is only one goal: only for testing purposes 
		 */
		private void truth() {  //make sure that only the goal is the goal
			for(int i = 0; i < this.field.size(); i++) {
				for(int j = 0; j < this.field.get(i).size(); j++){
					if(this.field.get(i).get(j).isGoal()) {
						System.out.println("1");}
					else {
						System.out.println("0");}
				}
			}
		}
		
		/**
		 * prints the number of neighbors of each node for testing purposes
		 */
		void countNeighbors() { //count neighbors of each node
			for(int i = 0; i < this.field.size(); i++) {
				String x = "";
				for(int j = 0; j < this.field.get(i).size(); j++){
					x += " "+ (this.field.get(i).get(j).getNeighbors().size());
				}
				System.out.println(x);
			}
		}
		
		/**
		 * testing method
		 */
		void testRide(){
			this.toString();
			System.out.println("testing build");
			this.build();
			System.out.println("truth table");
			this.truth();
			System.out.println("testing edit");
			this.edit();
			System.out.println("testing swoll");
			this.swoll(3, 3, 4, 3);
			this.toString();
			System.out.println("testing smoll");
			this.smoll(3, 3, 4, 3);
			this.toString();
			System.out.println("testing the function to return a random node");
			node x = this.randNode();
			System.out.println(x.getX());
			System.out.println(x.getY());
			System.out.println("testing the function to find distances between two nodes");
			this.countNeighbors();
		 }
	

}		


